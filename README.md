# Bot-Playground

Please refer to the documentation of our [playground project on GitHub](https://github.com/Refactoring-Bot/Bot-Playground). This repository is used to test the [Refactoring-Bot](https://github.com/Refactoring-Bot/Refactoring-Bot) on a GitLab project.  The preferred way to try the bot is to fork from this repository and let the bot work on the fork.

Feature requests, bug reports and questions can be created informally via [issues on GitHub](https://github.com/Refactoring-Bot/Refactoring-Bot/issues).
